package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;

import static org.academiadecodigo.bootcamp67.MainMenuScreen.*;

public class GameScreen implements Screen {

    private MakeAWish game;
    private Stage stage;

    private Texture nose;
    private Texture player;
    private Texture block;
    private Texture block2;
    private Texture block3;
    private Rectangle playerHitbox;
    private Rectangle blockHitbox;
    private Rectangle block2Hitbox;
    private Rectangle block3Hitbox;

    private Texture enemy;
    private Rectangle enemyHitbox;

    private Texture foreground1;
    private Texture foreground2;
    private Texture background3;
    private Texture background4;
    private Texture background5;
    private Texture background6;
    private Texture background7;
    private Texture background8;
    private Texture background9;
    private Texture background10;

    private Sound squishSound;
    private Sound WEEEEE;
    private Sound jumpSound;
    private Sound catchCricket;

    private Music gameMusic;
    private OrthographicCamera camera;

    private float blockX = 0;
    private float blockY = 0;
    private float blockWidth = WIDTH;
    private float blockHeight = 120;

    private float block2X = 500;
    private float block2Y = 250;
    private float block2Width = 200;
    private float block2Height = 120;

    private float block3X = 800;
    private float block3Y = 350;
    private float block3Width = 200;
    private float block3Height = 120;

    private float speed = 200.0f;
    private float playerW = 150;
    private float playerH = 150;
    private float playerX = 0;
    private float playerY = 52;
    private float velocity = 0.0f;
    private float gravity = 1.0f;
    private int state = 0;

    private float finalw = 400;
    private float finalh = 200;
    private float finaly = 70;
    private float finalx = 1100;
    private Texture finalBoss;
    private Rectangle finalBossHitbox;

    private float enemyW = 100;
    private float enemyH = 100;
    private float enemyX = 530;
    private float enemyY = 335;

    public GameScreen(final MakeAWish game) {
        this.game = game;

        loadTextures();

        //squishSound = Gdx.audio.newSound(Gdx.files.internal(""));
        //catchCricket = Gdx.audio.newSound(Gdx.files.external(""));
        this.WEEEEE = Gdx.audio.newSound(Gdx.files.internal("music/WEEEEE! - Sound effect..mp3"));
        this.jumpSound = Gdx.audio.newSound(Gdx.files.internal("music/jump.mp3"));
        this.gameMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Pinocchio OST - 11 - I've Got No Strings.mp3"));

        // create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, WIDTH, HEIGHT);

    }

    public void loadTextures() {
        background10 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/10_Sky_G.png"));
        background9 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/09_Forest_G.png"));
        background8 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/08_Forest_G.png"));
        background7 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/07_Forest_G.png"));
        background6 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/06_Forest_G.png"));
        background5 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/05_Particles_G.png"));
        background4 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/04_Forest_G.png"));
        background3 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/03_Particles_G.png"));
        foreground2 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/02_Bushes_G.png"));
        foreground1 = new Texture(Gdx.files.internal("Parallax Forest Background - Green/01_Mist_G.png"));
    }

    public void renderBackground() {
        Texture[] backgrounds = new Texture[]{background10, background9, background8, background7, background6, background5, background4, background3};
        for (Texture background : backgrounds) {
            game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }

    public void renderForeground() {
        game.batch.draw(foreground2, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.draw(foreground1, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {

        ScreenUtils.clear(0, 0, 0.2f, 1);

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        renderBackground();

        stage.draw();
        game.batch.draw(nose, 0, 700, 300, 100);
        game.batch.draw(finalBoss, finalx, finaly, finalw, finalh);
        game.batch.draw(player, playerX, playerY, playerW, playerH);
        game.batch.draw(block, blockX, blockY, blockWidth, blockHeight);
        game.batch.draw(block2, block2X, block2Y, block2Width, block2Height);
        game.batch.draw(block3, block3X, block3Y, block3Width, block3Height);
        game.batch.draw(enemy, enemyX, enemyY, enemyW, enemyH);

        renderForeground();

        game.batch.end();

        playerHitbox.set(playerX, playerY, playerW, playerH);
        blockHitbox.set(blockX, blockY, blockWidth, blockHeight);
        block2Hitbox.set(block2X, block2Y, block2Width, block2Height);
        block3Hitbox.set(block3X, block3Y, block3Width, block3Height);
        enemyHitbox.set(enemyX, enemyY, enemyW, enemyH);
        finalBossHitbox.set(finalx, finaly, finalw, finalh);

        if (playerX > finalx && playerX < WIDTH && playerY < finalh) {
            game.setScreen(new GameWonScreen(game));
            dispose();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            playerY -= Gdx.graphics.getDeltaTime() * speed;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            playerX -= Gdx.graphics.getDeltaTime() * speed;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            playerX += Gdx.graphics.getDeltaTime() * speed;
        }

        if (state == 1) {
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                velocity -= 5.0;
                jumpSound.play();
                WEEEEE.play();
            }
            if (playerY < 0) {
                playerY = Gdx.graphics.getHeight() / 1000;
                velocity = 0;
                state = 0;
            } else {
                velocity = velocity + gravity;
                playerY = playerY - velocity;
            }

        } else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
                state = 1;
            }
        }

        if (!playerHitbox.overlaps(block2Hitbox)) {

        } else {
            playerY += 3;
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // start the playback of the background music
        // when the screen is shown
        this.gameMusic.setLooping(true);
        this.gameMusic.play();
        nose = new Texture("nose/nose.png");
        finalBoss = new Texture("geppetto_final.png");
        this.player = new Texture("pinocchio/001_pinocchio.png");
        playerHitbox = new Rectangle(playerX, playerY, playerW, playerH);
        this.block = new Texture("platforms/big_platform.png");
        blockHitbox = new Rectangle(blockX, blockY, blockWidth, blockHeight);
        this.block2 = new Texture("platforms/small_platform_1.png");
        block2Hitbox = new Rectangle(block2X, block2Y, block2Width, block2Height);
        this.block3 = new Texture("platforms/small_platform_2.png");
        block3Hitbox = new Rectangle(block3X, block3Y, block3Width, block3Height);
        enemyHitbox = new Rectangle(enemyX, enemyY, enemyW, enemyH);
        finalBossHitbox = new Rectangle(finalx, finaly, finalw, finalh);
        this.enemy = new Texture("Enemies/tambor.png");
        this.stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        jumpSound.dispose();
        WEEEEE.dispose();
        gameMusic.dispose();
        player.dispose();
        block.dispose();
        block2.dispose();
        block3.dispose();
        enemy.dispose();
        finalBoss.dispose();
    }
}