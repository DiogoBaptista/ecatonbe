package org.academiadecodigo.bootcamp67;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
