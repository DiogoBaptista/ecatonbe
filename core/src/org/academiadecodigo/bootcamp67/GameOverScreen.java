package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.ScreenUtils;

import static org.academiadecodigo.bootcamp67.MainMenuScreen.HEIGHT;
import static org.academiadecodigo.bootcamp67.MainMenuScreen.WIDTH;

public class GameOverScreen implements Screen {

    private MakeAWish game;
    private OrthographicCamera camera;

    public GameOverScreen(MakeAWish game) {
        this.game = game;

        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, WIDTH, HEIGHT);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0.2f, 1);

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
