package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;

import static org.academiadecodigo.bootcamp67.MainMenuScreen.HEIGHT;
import static org.academiadecodigo.bootcamp67.MainMenuScreen.WIDTH;

public class GameWonScreen implements Screen {

    private MakeAWish game;
    private OrthographicCamera camera;

    private Music winMusic;
    private Texture background;

    public GameWonScreen(MakeAWish game) {
        this.game = game;

        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, WIDTH, HEIGHT);
        this.winMusic = Gdx.audio.newMusic(Gdx.files.internal("music/WHEN YOU WISH UPON A STAR (Chill TRAP REMIX).mp3"));
        this.background = new Texture(Gdx.files.internal("VictoryScreen.png"));

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0.2f, 1);

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        winMusic.play();
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
