package org.academiadecodigo.bootcamp67;

public class Position {
    private int rows;
    private int cols;
    private Field grid;

    public Field getGrid() {
        return grid;
    }

    public Position(int cols, int rows, Field field) {
        grid = field;
        this.rows = rows;
        this.cols = cols;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows += rows;
    }

    public int getCols()    {
        return cols;
    }

    public void setCols(int cols) {
        this.cols += cols;
    }

    public int collumToX(int cols) {
        return cols * Field.cellSize + Field.PADDING;
    }

    public int rowToY(int rows) {
        return rows * Field.cellSize + Field.PADDING;
    }

    public int getX() {
        return cols * Field.cellSize + Field.PADDING;
    }

    public int getY() {
        return rows * Field.cellSize + Field.PADDING;
    }

}
