package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.math.Rectangle;

import javax.swing.text.Position;

public class Field {
    public static final int PADDING = 10;
    public static final int cellSize = 10;
    public static final int COLS = 10;
    public static final int ROWS = 10;
    public static Position playerPos;
    private Rectangle grid;

    public Field() {
        grid = new Rectangle(PADDING, PADDING, (collumToX(COLS) - PADDING), (rowToY(ROWS) - PADDING));
    }

    public void init() {
    }

    public float getWidth() {
        return grid.getWidth();
    }


    public float getHeight() {
        return grid.getHeight();
    }


    public int collumToX(int cols) {
        return cols * Field.cellSize + Field.PADDING;
    }

    public int rowToY(int rows) {
        return rows * Field.cellSize + Field.PADDING;
    }

    public float getX() {
        return grid.getX();
    }

    public float getY() {
        return grid.getY();
    }

    public static void setPlayerPos(Position playerPos) {
        Field.playerPos = playerPos;
    }

}
