package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ScreenUtils;


public class MainMenuScreen implements Screen {

    private final MakeAWish game;
    private final OrthographicCamera camera;
    private Animation<TextureRegion> animation;
    private float elapsed;

    private final Texture playImage;
    private final Texture quitImage;
    private final Texture soundImage;
    private final Texture controlsImage;

    private Rectangle sound;
    private Rectangle play;
    private Rectangle quit;
    private Rectangle controls;

    private final Sound playSound;
    private final Music menuMusic;

    public static final int WIDTH = 1420;
    public static final int HEIGHT = 800;

    public MainMenuScreen(final MakeAWish game) {
        this.game = game;

        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, WIDTH, HEIGHT);

        this.playImage = new Texture(Gdx.files.internal("buttons/play_bt.png"));
        this.quitImage = new Texture(Gdx.files.internal("buttons/quit_bt.png"));
        this.soundImage = new Texture(Gdx.files.internal("buttons/sound_bt.png"));
        this.controlsImage = new Texture(Gdx.files.internal("buttons/controls_bt.png"));

        this.playSound = Gdx.audio.newSound(Gdx.files.internal("music/GameStart.mp3"));
        this.menuMusic = Gdx.audio.newMusic(Gdx.files.internal("music/NEW DISNEY INTRO WITHOUT TEXT NO COPYRIGHT.mp3"));
        this.menuMusic.setLooping(true);
        this.menuMusic.play();

        createItems();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        ScreenUtils.clear(0, 0, 0.2f, 1);

        this.camera.update();
        this.game.batch.setProjectionMatrix(this.camera.combined);

        batchBeginAndEnd();

        if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {

            Vector2 touch = new Vector2(Gdx.input.getX(), -Gdx.input.getY() + Gdx.graphics.getHeight());

            playGame(touch);
            showControls(touch);
            toggleSound(touch);
            quitGame(touch);

        }
    }

    private void batchBeginAndEnd() {
        elapsed += Gdx.graphics.getDeltaTime();
        this.game.batch.begin();

        this.game.batch.draw(animation.getKeyFrame(elapsed), 0f, 0f, WIDTH, 800);
        this.game.batch.draw(this.playImage, this.play.x, this.play.y, this.play.width, this.play.height);
        this.game.batch.draw(this.quitImage, this.quit.x, this.quit.y, this.quit.width, this.quit.height);
        this.game.batch.draw(this.soundImage, this.sound.x, this.sound.y, this.sound.width, this.sound.height);
        this.game.batch.draw(this.controlsImage, this.controls.x, this.controls.y, this.controls.width, this.controls.height);

        this.game.batch.end();
    }

    private void showControls(Vector2 touch) {
        if (this.controls.contains(touch)) {
            this.game.setScreen(new ControlsScreen(this.game, this));
        }
    }

    private void playGame(Vector2 touch) {
        if (this.play.contains(touch)) {
            this.playSound.play();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.game.setScreen(new GameScreen(this.game));
            dispose();
        }
    }

    private void toggleSound(Vector2 touch) {
        if (this.sound.contains(touch)) {
            if (!this.menuMusic.isPlaying()) {
                this.menuMusic.play();
                this.menuMusic.setLooping(true);
                return;
            }
            this.menuMusic.pause();
        }
    }

    private void quitGame(Vector2 touch) {
        if (this.quit.contains(touch)) {
            System.exit(1);
        }
    }

    private void createItems() {
        this.animation = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("IntroFinal2.gif").read());

        this.sound = new Rectangle();
        this.sound.x = WIDTH - 256;
        this.sound.y = HEIGHT - 96;
        this.sound.width = 256;
        this.sound.height = 96;

        this.play = new Rectangle();
        this.play.x = WIDTH / 2 - 256 / 2;
        this.play.y = HEIGHT / 2 + 96;
        this.play.width = 256;
        this.play.height = 96;

        this.controls = new Rectangle();
        this.controls.x = WIDTH / 2 - 256 / 2;
        this.controls.y = HEIGHT / 2;
        this.controls.width = 256;
        this.controls.height = 96;

        this.quit = new Rectangle();
        this.quit.x = WIDTH / 2 - 256 / 2;
        this.quit.y = (HEIGHT / 2) - 96;
        this.quit.width = 256;
        this.quit.height = 96;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.playImage.dispose();
        this.quitImage.dispose();
        this.controlsImage.dispose();
        this.soundImage.dispose();
        this.menuMusic.dispose();
    }

}

