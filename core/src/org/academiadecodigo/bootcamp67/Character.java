package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.ApplicationAdapter;

public abstract class Character extends ApplicationAdapter {


    private int health;
    private int dmg;
    private Direction currentDirection;
    private Position position;

    public abstract int dmgDealt();

    public abstract void dmgReceived(int dmg);

    public Character(int health, int dmg) {
        this.health = health;
        this.dmg = dmg;

    }

    public void setCurrentDirection(Direction direction) {
        this.currentDirection = direction;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public boolean isDead() {
        if (health < 1) {
            return true;
        }
        return false;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Direction getCurrentDirection() {
        return currentDirection;
    }
}



