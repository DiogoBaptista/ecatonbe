package org.academiadecodigo.bootcamp67;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class DesktopLauncher {
    public static final int WIDTH = 1420;
    public static final int HEIGHT = 800;
    public static final int FPS = 60;

    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("MakeAWish");
        config.setWindowedMode(WIDTH, HEIGHT);
        config.useVsync(true);
        config.setForegroundFPS(FPS);
        new Lwjgl3Application(new MakeAWish(), config);
    }
}